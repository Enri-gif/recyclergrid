package com.example.grid;

public class Matrix {
    public int arr[][];
    public int size;

    // Initialize the matrix
    public Matrix(int size)
    {
        int i,j;
        this.size = size;
        arr = new int[size][size];
        for(i = 0; i < size; i++)
        {
            for(j = 0; j < size; j++)
            {
                if (i < size && (i == j-1 || i == j+1))
                {
                    arr[i][j] = 1;
                }//for unenven matrix
                else
                    arr[i][j] = 0;
            }
        }
    }

    // Add edges
    public void addEdge(int i, int j)
    {
        arr[i][j] = 1;
        arr[j][i] = 1;
    }

    // Remove edges
    public void removeEdge(int i, int j)
    {
        arr[i][j] = 0;
        arr[j][i] = 0;
    }

    // Print the matrix
    /*
    public String toString()
    {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < size; i++)
        {
            s.append(i + ": ");
            for (int j : arr[i])
            {
                s.append((j ? 1 : 0) + " ");
            }
            s.append("\n");
        }
        return s.toString();
    }*/
}
