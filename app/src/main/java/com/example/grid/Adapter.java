package com.example.grid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {
    ArrayList<Integer> matrix;
    Context context;
    LayoutInflater inflater;

    public Adapter(Context ctx, ArrayList<Integer> mtx){
        matrix = mtx;
        context = ctx;
        inflater = LayoutInflater.from(ctx);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.grid, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.state.setText(matrix.get(position).toString());
    }

    @Override
    public int getItemCount() {
        return matrix.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView state;
        public ViewHolder(@NonNull View view){
            super(view);
            state = view.findViewById(R.id.cardText);
        }
    }
}
