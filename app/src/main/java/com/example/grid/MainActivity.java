package com.example.grid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.lang.Boolean;
import java.util.stream.Collectors;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<Integer> grid;
    Adapter adapter;
    Matrix matrix;
    Dij dij;
    TextView steps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        steps = findViewById(R.id.steps);
        recyclerView = findViewById(R.id.recycler);
        grid = new ArrayList<>();

        matrix = new Matrix(8);
        dij = new Dij();
        dij.CalcDist(matrix.arr, 0, 5);
        for (int i = 0; i < matrix.arr.length; i++){
            for (int j = 0; j < matrix.arr[i].length; j++){
                grid.add(matrix.arr[i][j]);
            }
        }
        steps.setText(dij.dist);
        adapter = new Adapter(this, grid);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, matrix.size, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(adapter);
    }
}